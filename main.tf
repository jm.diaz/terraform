terraform {
    required_version = "1.2.4"
    required_providers {
        aws = {     
            source  = "hashicorp/aws"
            version = "= 4.22"
        }
    }
}

# Configure the AWS Provider
provider "aws" {
    region = "us-east-1"
}

resource "aws_s3_bucket" "b" {
  bucket = var.bucket_name
}

resource "aws_s3_bucket_public_access_block" "example" {
    bucket = aws_s3_bucket.b.id
    restrict_public_buckets = true
    ignore_public_acls = true
    block_public_acls   = true
    block_public_policy = true
}